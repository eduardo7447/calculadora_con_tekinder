﻿from tkinter import *
import tkinter

ventana = Tk()
ventana.title( "Calculadora")
ventana.geometry("300x300")
#iniciar indice de escritura
i = 0
##ventana.geometry("600x500")
# funciones

def click_boton(valor):
    global i
    e_texto.insert(i,valor)
    i += 1

def borrar_todo():

    e_texto.delete(0, END) # 0 es el indice donde comienza a borrar
    i = 0
def borrar_elemento():
    #global i no es tan recomendable ya  que el conteo de los caracteres hay que actualizarlos manualmente

    largo = e_texto.get()  #modo de guardar el contenido de la caja de texto en una variable con get()
    e_texto.delete(len(largo)-1)

    #utilizamos el indice de la variable global
    #i-=1 no es tan recomendable ya  que el conteo de los caracteres hay que actualizarlos manualmente
def resultado() :
    ecuacion = e_texto.get()
    valores = eval(ecuacion)
    e_texto.delete(0,"end")
    e_texto.insert(0,valores)
    i = 0


# Entrada--- declara variable para la entrada de texto con su fuente
e_texto = Entry(ventana, font = "console 18 ")
e_texto.grid(row = 0, column = 0 ,columnspan = 20, padx= 5, pady = 2) # ubica el texto en la pantalla



# Dibujar botones de numeros
num1 = Button(ventana, text = "1", width = 5, height = 2, command = lambda : click_boton(1))
num2 = Button(ventana, text = "2", width = 5, height = 2, command = lambda : click_boton(2))
num3 = Button(ventana, text = "3", width = 5, height = 2, command = lambda : click_boton(3))
num4 = Button(ventana, text = "4", width = 5, height = 2, command = lambda : click_boton(4))
num5 = Button(ventana, text = "5", width = 5, height = 2, command = lambda : click_boton(5))
num6 = Button(ventana, text = "6", width = 5, height = 2, command = lambda : click_boton(6))
num7 = Button(ventana, text = "7", width = 5, height = 2, command = lambda : click_boton(7))
num8 = Button(ventana, text = "8", width = 5, height = 2, command = lambda : click_boton(8))
num9 = Button(ventana, text = "9", width = 5, height = 2, command = lambda : click_boton(9))
num0 = Button(ventana, text = "0", width = 5, height = 2, command = lambda : click_boton(0))

#  dibujar botones de simbolos
resta=      Button(ventana, text = "-", width = 5, height = 2, command = lambda : click_boton("-"))
suma=       Button(ventana, text = "+", width = 5, height = 2, command = lambda : click_boton("+"))
multi=      Button(ventana, text = "x", width = 5, height = 2, command = lambda : click_boton("* "))
division=   Button(ventana, text = "/", width = 5, height = 2, command = lambda : click_boton("/"))
igual=      Button(ventana, text = "=", width = 5, height = 2, command = lambda : resultado())
parentIzquierdo= Button(ventana, text = "(", width = 5, height = 2, command = lambda : click_boton("("))
parentDerecho= Button(ventana, text = ")", width = 5, height = 2, command = lambda : click_boton(")"))
punto=          Button(ventana, text = ".", width = 5, height = 2, command = lambda : click_boton("."))


# Botones de borrado parcial  y total
Ac = Button(ventana, text = "Ac", width = 5, height = 2, command = lambda : borrar_todo())
Del = Button(ventana, text = "Del", width = 5, height = 2, command = lambda : borrar_elemento())
##
# ubicar Botones en pantalla
Ac.grid     (row =1, column= 5, padx= 5, pady = 2)
Del.grid    (row = 1, column= 4, padx= 5, pady = 2)

num9.grid   (row = 1,column = 3, padx= 5 , pady = 2)
num8.grid   (row = 1,column = 2, padx= 5 , pady = 2)
num7.grid   (row = 1,column = 1, padx= 5 , pady = 2)

num6.grid   (row = 2,column = 3, padx= 5 , pady = 2)
num5.grid   (row = 2,column = 2, padx= 5 , pady = 2)
num4.grid   (row = 2,column = 1, padx= 5 , pady = 2)

num3.grid   (row = 3,column = 3, padx= 5 , pady = 2)
num2.grid   (row = 3,column = 2, padx= 5 , pady = 2)
num1.grid   (row = 3,column = 1, padx= 5 , pady = 2)

suma.grid       (row=2, column = 4 ,padx = 5 ,pady = 2)
resta.grid      (row=2, column = 5 ,padx = 5 ,pady = 2 )
multi.grid      (row=3, column = 4 ,padx = 5 ,pady = 2 )
division.grid   (row=3, column = 5 ,padx = 5 ,pady = 2 )

num0.grid               (row = 4,column = 1, padx= 2 , pady = 2)
parentIzquierdo.grid    (row = 4,column = 2, padx = 2 , pady = 2)
parentDerecho.grid      (row = 4,column = 3, padx = 2 , pady = 2)
igual.grid              (row = 4,column = 4, padx = 2 , pady = 2)
punto.grid              (row = 4,column = 5, padx = 2 , pady = 2)
##
# iniciar ventana
ventana.mainloop()